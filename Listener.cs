﻿using System;
using Apache.NMS;
using Apache.NMS.Util;
using ActiveMQ_JBHunt;
using Apache.NMS.Stomp;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Collections;

namespace ActiveMQ_JBHunt
{
    public class Listener
    {
        public static bool Start()
        {
            try
            {

                ActiveMQSettings settings = ConfigurationProvider.Instance.BrokerSettings;
                bool connected = false;

                IConnectionFactory factory = new NMSConnectionFactory(new Uri(settings.BrokerURL));
                IConnection connection = factory.CreateConnection(settings.UserID, settings.Password);

                int times = 0;
                while (!connected)
                {
                    Console.WriteLine("Trying " + times.ToString());
                    try
                    {
                        times++;
                        connection.Start();
                        connected = connection.IsStarted;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        connection.Dispose();
                        connection = factory.CreateConnection(settings.UserID, settings.Password);
                    }
                }

                connection.Start();
                ISession session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
                IDestination dest = session.GetDestination(settings.CustomerRequestSubscription);
                // GetTopic(settings.CustomerRequestTopic);
                int count = 0;
                using (IMessageConsumer consumer = session.CreateConsumer(dest))
                {
                    DateTime start = DateTime.Now;
                    Console.WriteLine("Looking for messages...");

                    IMessage msg = consumer.Receive();
                    while (true)
                    {

                        if (msg is ITextMessage)
                        {
                            ITextMessage txtMsg = msg as ITextMessage;
                            String body = txtMsg.Text;

                            XmlDocument doc = new XmlDocument();
                            doc.PreserveWhitespace = true;
                            doc.LoadXml(body);

                            //StreamReader sr = new StreamReader(filename);
                            //String line = sr.ReadToEnd();

                            //XmlDocument doc = new XmlDocument();
                            //doc.PreserveWhitespace = true;
                            //doc.LoadXml(line);

                            XmlElement root = doc.DocumentElement;
                            XmlNodeList elemList = root.GetElementsByTagName("Event");

                            IEnumerator ienum = elemList.GetEnumerator();
                            while (ienum.MoveNext())
                            {
                                XmlNode title = (XmlNode)ienum.Current;
                                if (title.HasChildNodes)
                                {
                                    for (int i = 0; i < title.ChildNodes.Count; i++)
                                    {
                                        if (title.ChildNodes[i].LocalName == "#whitespace")
                                        {

                                        }
                                        else
                                        {
                                            Console.WriteLine(title.ChildNodes[i].LocalName + ": " + title.ChildNodes[i].InnerText);
                                        }
                                    }
                                }

                            }

                           


                            XmlNodeList girlAddress = doc.GetElementsByTagName("gAddress");
                            XmlNode xx = doc.ChildNodes[2];
                            // XmlNode.root = root.FirstChild;
                            //Display the contents of the child nodes.
                            if (doc.HasChildNodes)
                            {
                                for (int i = 0; i < doc.ChildNodes.Count; i++)
                                {
                                    Console.WriteLine(doc.ChildNodes[i].InnerText);
                                }
                            }


                            //Console.WriteLine(doc.DocumentElement.OuterXml);


                            //XmlRootAttribute xRoot = new XmlRootAttribute();
                            //xRoot.ElementName = @"@targetNamespace";
                            //xRoot.Namespace = @"http://jbhunt.com/OBC/Compliance/Schema/Standard/Event";
                            //xRoot.IsNullable = true;

                            //var serializer = new XmlSerializer(typeof(StatusDocumentItem));
                            //StatusDocumentItem result;

                            //using (TextReader reader = new StringReader(body))
                            //{
                            //    result = (StatusDocumentItem)serializer.Deserialize(reader);
                            //}

                            //Console.WriteLine(result.Message);
                            Console.ReadKey();
                            if ("SHUTDOWN".Equals(body))
                            {
                                TimeSpan diff = DateTime.Now - start;
                                Console.WriteLine(String.Format("Received {0} in {1} seconds", count, (1.0 * diff.TotalMilliseconds / 1000.0)));
                                break;
                            }
                            else
                            {
                                if (count == 0)
                                {
                                    start = DateTime.Now;
                                }
                                count++;
                                if (count % 1000 == 0)
                                {
                                    Console.WriteLine(String.Format("Received {0} messages.", count));
                                }
                            }

                        }
                        else
                        {
                            Console.WriteLine("Unexpected message type: " + msg.GetType().Name);
                        }
                    }


                }
                connection.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                throw;
            }

            return false;

        }

        public class MyType1
        {
            private string _Label;
            public string SentDateTime
            {
                set { _Label = value; }
                get { return _Label; }
            }

            private int _Epoch;
            public int VehicleNumber
            {
                set { _Epoch = value; }
                get { return _Epoch; }
            }
        }
        public class NamespaceIgnorantXmlTextReader : XmlTextReader
        {
            public NamespaceIgnorantXmlTextReader(System.IO.TextReader reader) : base(reader) { }

            public override string NamespaceURI
            {
                get { return ""; }
            }
        }

        // helper class to omit XML decl at start of document when serializing
        public class XTWFND : XmlTextWriter
        {
            public XTWFND(System.IO.TextWriter w) : base(w) { Formatting = System.Xml.Formatting.Indented; }
            public override void WriteStartDocument() { }
        }

        [XmlRoot]
        public class StatusDocumentItem
        {
            [XmlElement]
            public string ElogEvent;
            [XmlElement]
            public string LastUpdated;
            [XmlElement]
            public string Message;
            [XmlElement]
            public int State;
            [XmlElement]
            public string StateName;
        }

      


    }

}
