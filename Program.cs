﻿using System;

using System.Threading;
using ActiveMQ_JBHunt;

namespace ActiveMQ_JBHunt
{
    class Program
    {
        static void Main(string[] args)
        {
            while (Listener.Start())
            {
                Console.WriteLine("Successfully read message");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Finished");
        }


    }
}
