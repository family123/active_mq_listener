﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Azure.KeyVault;

namespace ActiveMQ_JBHunt
{
    public class ConfigurationProvider
    {
        private static ConfigurationProvider _instance;
        public static ConfigurationProvider Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ConfigurationProvider();
                    _instance.Prime();
                    // _instance.BrokerURL();
                }

                return _instance;
            }
        }

        public ActiveMQSettings BrokerSettings { get; private set; }
        public ConfigurationProvider configuration { get; set; }

        private ConfigurationProvider()
        {
        }


        private void Prime()
        {

            ActiveMQSettings amqSettings = new ActiveMQSettings();
            amqSettings.ReadSettings();
            BrokerSettings = amqSettings;

        }
    }

    public class ActiveMQSettings
    {
        public string UserID { get; set; }
        public string Password { get; set; }
        public string BrokerURL { get; set; }
        public string CustomerRequestTopic { get; set; }
        public string CustomerRequestSubscription { get; set; }

        private static String Env(String key, String defaultValue)
        {
            String rc = System.Environment.GetEnvironmentVariable(key);
            if (rc == null)
            {
                return defaultValue;
            }
            return rc;
        }
        public void ReadSettings()
        {
            UserID = Credentials_UserID();
            Password = Credentials_Pass();
            BrokerURL = BrokerURL1();
            //BrokerURL = Env("BrokerURL", "activemq:failover:(tcp://amq-obc-tst1.jbhunt.com:61616,tcp://amq-obc-tst2.jbhunt.com:61616)?transport.randomize=false&transport.startupMaxReconnectAttempts=1&connection.watchTopicAdvisories=false&connection.AsyncSend=true&connection.prefetchSize=1");
            CustomerRequestTopic = Env("CustomerTopic", "OBC.T.FAULTCODE.INBOUND.EVENT");
            CustomerRequestSubscription = Env("Subscription", "SUB.OBC.PROCESS.OBC.T.FAULTCODE.INBOUND.EVENT");

        }
        public string BrokerURL1()
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, $"http://configuration-test.jbhunt.com/ws_infrastructure_configuration/listener_ordermanagement_unifiedcustomerrequest/PROF3");
            HttpResponseMessage plansResponse = client.SendAsync(request).Result;
            string plansResult = plansResponse.Content.ReadAsStringAsync().Result;
            Newtonsoft.Json.JsonConvert.DeserializeObject(plansResult);
            dynamic data = JObject.Parse(plansResult);
            string value = (string)data["propertySources"][2]["source"]["jbhunt.general.jms.activeMQ.connectionFactory.obc.consumer.brokerURL"];
            value = (value.Trim(new Char[] { ' ', '$', '{', '}', '.' }));
            value = (value.Replace("nio", "tcp"));
            //string removeString = "";

            int index = value.IndexOf("(") + "(".Length;
            int index2 = value.LastIndexOf(")");
            string result = value.Substring(index, index2 - index);
            value = "activemq:failover:(" + result + ")?transport.randomize=false&transport.startupMaxReconnectAttempts=1&connection.watchTopicAdvisories=true&connection.AsyncSend=true&consumer.prefetchPolicy.all=1";

            //if (index > 0)
            //    removeString = value.Substring(0, index);

            ////int index2 = value.IndexOf(removeString);
            //string str = value.Substring(0, value.IndexOf(removeString) + removeString.Length);
            //value = "activemq" + value.Substring(str.Length).Replace(removeString, "");
            return value;

        }
        public string Credentials_UserID()
        {
            // Fetch configuration values

            var adClientId = "7bfcca30-2b18-4ade-9e5b-3801a4760d98";

            var adKey = "kPwucalCmv3nXGMO3FgalUsDw1A8sWID0ra5Sxjh0us=";

            var keyUrl = "https://activemq-jbhunt.vault.azure.net/secrets/appsettings--connectionStrings--ActiveMQ-JBHunt/d2e2505113024d30b4dc706e6ec21f21";

            var keyVault = new KeyVaultClient(async (string authority, string resource, string scope) => {

                var authContext = new AuthenticationContext(authority);

                var credential = new ClientCredential(adClientId, adKey);

                var token = await authContext.AcquireTokenAsync(resource, credential);

                return token.AccessToken;

            });

            // Get the API key out of the vault

            var apiKey = keyVault.GetSecretAsync(keyUrl).Result.Value;
            return apiKey;

        }
        public string Credentials_Pass()
        {
            // Fetch configuration values

            var adClientId = "7bfcca30-2b18-4ade-9e5b-3801a4760d98";

            var adKey = "kPwucalCmv3nXGMO3FgalUsDw1A8sWID0ra5Sxjh0us=";

            var keyUrl = "https://activemq-jbhunt.vault.azure.net/secrets/appSettings--connectionStrings--ActiveMQ-JBHuntpass/d1c25fe1486d4572944375f1c1f51031";

            var keyVault = new KeyVaultClient(async (string authority, string resource, string scope) => {

                var authContext = new AuthenticationContext(authority);

                var credential = new ClientCredential(adClientId, adKey);

                var token = await authContext.AcquireTokenAsync(resource, credential);

                return token.AccessToken;

            });

            // Get the API key out of the vault

            var apiKey = keyVault.GetSecretAsync(keyUrl).Result.Value;
            return apiKey;

        }

    }
}




